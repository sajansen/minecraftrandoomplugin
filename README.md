# MinecraftRandoomPlugin

_Bringing some random Doom to players. Muwahaha!_

## Development


### Setup 
- Run BuildTools.jar from Spigot
- Add the <buildtools_dir>/Spigot/Spigot-API/target/spigot-api-1.14.4-R0.1-SNAPSHOT-shaded.jar as dependency to the project
- Build artifact
- Run <buildtools_dir>/spigot-1.14.4.jar in a separate directory to start the server and create the plugins/ folder
- Add generated artifact JAR to the plugins folder of your server

### Dooms

Dooms are scheduled at random times by `RandoomTaskSpawner.spawnDoom()`, using the BukkitRunnable. 

Whenever a Doom is scheduled, `RandoomClass.schedule()` will be called to schedule this Doom. `RandoomClass.run()` is called to finally execute the Doom.

The timeline is as follows:

```
              doom execution
                     |
                     V
      spawntime            nextcreationtime         spawntime
|------------------->|--------------------------->|----------->...
                      \---------->|
                      doom duration
```

1. `RandoomTaskSpawner.spawnDoom()` creates a new Doom, which will be executed after `spawntime`. 
1. `RandoomTaskSpawner.schedule()` schedules a new `RandoomTaskSpawner` after `spawntime + nextreactiontime` to create a new Doom.
1. When the Doom executes, it can give Doom right away or it may have its own `duration`. This execution will be done asynchronously from the `RandoomTaskSpawner` spawner. 


##### Listeners

A Doom Listener must extend the `RandoomListener` class.

When a Doom uses a Listener, the `maxDuration` must be set on the Listener. The Listener will unschedule itself after this duration. 

##### Enable / disable

To start spawning Dooms, simply call `Main.enableDoom();`. To disable Doom, call `Main.disableDoom()`. When adding a new Doom, make sure you first check for the `Main.isDoomEnabled()` boolean before executing the Doom. 

Each Doom extends `RandoomClass`. 


### Future

##### Custom DoomEvent

What can be added is the use of a custom DoomEvent, which will trigger after the Doom has been applied. With an EventListener, the Doom spanwer can spawn a new Doom. 

##### Countdown

Display a countdown preceding the Doom, just to horrify players even more.