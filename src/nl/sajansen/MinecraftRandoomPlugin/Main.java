package nl.sajansen.MinecraftRandoomPlugin;

import nl.sajansen.MinecraftRandoomPlugin.dooms.RandoomByChat;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Main class to be executed for this plugin. Here the plugin and its dooms are controlled.
 */
public class Main extends JavaPlugin {

    public static final int ticksPerSecond = 20;
    private static boolean doomEnabled = false;
    private final RandoomByChat randoomByChat;

    public Main() {
        randoomByChat = new RandoomByChat(this);
    }

    /**
     * Fired when the server enables the plugin
     */
    @Override
    public void onEnable() {
        getLogger().info("Randoom Plugin has been enabled");
        if (this.getConfig().getBoolean("doom.enabled"))
            enableDoom();
        else
            disableDoom();
    }

    /**
     * Fired when the server stops and disables all plugins
     */
    @Override
    public void onDisable() {
        getLogger().info("Randoom Plugin has been disabled");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!command.getName().equalsIgnoreCase("doom")) {
            return false;
        }

        if (args.length == 0) {
            sender.sendMessage("Please provide an argument");
            return false;
        }

        if (args[0].equalsIgnoreCase("enable")) {
            return onCommandEnableDoom(sender);
        }

        if (args[0].equalsIgnoreCase("disable")) {
            return onCommandDisableDoom(sender);
        }

        if (args[0].equalsIgnoreCase("spawn")) {
            return onCommandSpawnItems(sender, args);
        }

        if (args[0].equalsIgnoreCase("time")) {
            return onCommandTime(sender, args);
        }

        if (args[0].equalsIgnoreCase("config")) {
            if (args.length < 3) {
                sender.sendMessage("Please provide (an) extra argument(s)");
                return false;
            }

            if (args[1].equalsIgnoreCase("get"))
                return onCommandConfigGet(sender, args);
            if (args[1].equalsIgnoreCase("set"))
                return onCommandConfigSet(sender, args);
            else
                sender.sendMessage("Please provide an valid argument");
            return false;
        }

        return false;
    }

    private boolean onCommandTime(CommandSender sender, String[] args) {
        if (args.length != 2) {
            sender.sendMessage("The <value> argument is missing");
            return false;
        }

        int value;
        try {
            value = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            sender.sendMessage("Value must be a number");
            return false;
        }

        for (World world: this.getServer().getWorlds()) {
            world.setTime(value);
        }
        sender.sendMessage("Time set in all worlds");
        return true;
    }

    private boolean onCommandConfigSet(CommandSender sender, String[] args) {
        if (args.length != 4) {
            sender.sendMessage("The <value> argument is missing");
            return false;
        }

        String path = args[2];
        String value = args[3];

        String configValue = this.getConfig().getString(path);
        if (configValue == null) {
            sender.sendMessage("This config path doesn't exists");
            return false;
        }

        try {
            this.getConfig().set(path, Integer.parseInt(value));
        } catch (NumberFormatException e) {
            this.getConfig().set(path, value);
        } catch (Exception e) {
            sender.sendMessage(ChatColor.RED + "Something terrible happened during chancing this value...");
            return true;
        }
        sender.sendMessage("Value changed to: " + this.getConfig().getString(path));
        return true;
    }

    private boolean onCommandConfigGet(CommandSender sender, String[] args) {
        String path = args[2];
        String configValue = this.getConfig().getString(path);
        if (configValue == null) {
            sender.sendMessage("This config path doesn't exists");
            return true;
        }
        sender.sendMessage("Value: " + configValue);
        return true;
    }

    private boolean onCommandSpawnItems(CommandSender sender, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You must be a player to use this command");
            return false;
        }
        Player player = (Player) sender;

        if (args.length == 1) {
            player.sendMessage("Please provide an extra argument");
            return false;
        }

        int spawnAmount = 1;
        if (args.length == 3) {
            try {
                spawnAmount = Integer.parseInt(args[2]);
            } catch (Exception e) {
                player.sendMessage("Please provide an valid number as 'amount' argument");
            }
        }

        String spawnItemName = args[1].toUpperCase();
        Material spawnMaterial;
        try {
            spawnMaterial = Material.valueOf(spawnItemName);
        } catch (Exception e) {
            player.sendMessage("The requested entity doesn't exists");
            return false;
        }

        ItemStack spawnItemstack = new ItemStack(spawnMaterial, spawnAmount);
        player.getWorld().dropItemNaturally(player.getLocation(), spawnItemstack);
        return true;
    }

    private boolean onCommandDisableDoom(CommandSender sender) {
        if (!doomEnabled) {
            sender.sendMessage(ChatColor.DARK_GRAY + "DOOM has already been disabled");
            return true;
        }

        doomEnabled = false;
        sender.sendMessage(ChatColor.GRAY + "DOOM has been disabled");
        return true;
    }

    private boolean onCommandEnableDoom(CommandSender sender) {
        if (doomEnabled) {
            sender.sendMessage(ChatColor.DARK_GRAY + "DOOM has already been enabled");
            return true;
        }

        enableDoom();
        sender.sendMessage(ChatColor.GRAY + "DOOM has been enabled");
        return true;
    }

    public static boolean isDoomEnabled() {
        return doomEnabled;
    }

    public void enableDoom() {
        doomEnabled = true;
        RandoomTaskSpawner.start(this);
        randoomByChat.enable();
    }

    public void disableDoom() {
        doomEnabled = false;
        randoomByChat.disable();
    }
}
