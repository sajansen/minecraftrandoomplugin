package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;
import static nl.sajansen.MinecraftRandoomPlugin.Main.ticksPerSecond;

/**
 * Initiator for RandoomByMovementListener
 */
public class RandoomByMovement extends RandoomClass {

    private static RandoomByMovementListener currentListener;

    public RandoomByMovement(JavaPlugin plugin) {
        super(plugin);
    }

    @Override
    public String getName() {
        return "DOOM by Movement";
    }

    @Override
    public void run() {
        if (!isDoomEnabled()) {
            return;
        }

        if (currentListener != null) {
            currentListener.unregister();
        }

        currentListener = new RandoomByMovementListener(plugin, this);
        currentListener.register();

        int randoomDuration = plugin.getConfig().getInt("doom.types.RandoomByMovement.duration") * ticksPerSecond;
        currentListener.setMaxDuration(randoomDuration);

        plugin.getServer().broadcastMessage(discoColors("Let's DANCE, mothaf*ckers!"));
    }

    private String discoColors(String text) {
        StringBuilder output = new StringBuilder();
        for (char c : text.toCharArray()) {
            output.append(ChatColor.RESET);
            output.append(getRandomColor());
            output.append(c);
        }
        return output.toString();
    }

    private ChatColor getRandomColor() {
        int random = 6 + (int) (Math.random() * 10);
        return ChatColor.values()[random];
    }
}
