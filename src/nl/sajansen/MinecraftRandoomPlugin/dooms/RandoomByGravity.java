package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;
import static nl.sajansen.MinecraftRandoomPlugin.Main.ticksPerSecond;

/**
 * Every player will be doomed by being to heavy to jump on blocks. Falling will also cause extra damage to the knees
 */
public class RandoomByGravity extends RandoomClass {

    private static RandoomByMovementListener currentListener;

    public RandoomByGravity(JavaPlugin plugin) {
        super(plugin);
    }

    @Override
    public String getName() {
        return "DOOM by Gravity";
    }

    @Override
    public void run() {
        if (!isDoomEnabled()) {
            return;
        }

        int randoomDuration = plugin.getConfig().getInt("doom.types.RandoomByGravity.duration") * ticksPerSecond;
        for (Player player : plugin.getServer().getOnlinePlayers()) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, randoomDuration, -3));
        }

        plugin.getServer().broadcastMessage(ChatColor.YELLOW + "Yo mamma's so fat, she couldn't beat gravity!");
    }
}
