package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.regex.Pattern;

import static nl.sajansen.MinecraftRandoomPlugin.Main.ticksPerSecond;

/**
 * Listening to the chat. When forbiddenWords are spoken, the user will be doomed. HolyWords will be censored.
 */
public class RandoomByChatListener extends RandoomListener {

    private final RandoomByChat caller;

    public RandoomByChatListener(JavaPlugin plugin, RandoomByChat caller) {
        super(plugin);
        this.caller = caller;
    }

    public String getName() {
        return "RandoomByChatListener";
    }

    @EventHandler
    public void asyncPlayerChatEventDoom(AsyncPlayerChatEvent event) {
        String message = event.getMessage();
        List<String> forbiddenWords = plugin.getConfig().getStringList("doom.types.RandoomByChat.forbiddenWords");
        for (String forbiddenWord : forbiddenWords) {
            if (!Pattern.compile("(?i).*" + forbiddenWord + ".*").matcher(message).matches())
                continue;

            // Doom player
            event.getPlayer().sendMessage(ChatColor.RED + "Watch your words!");
            caller.setPlayerOnFire(event.getPlayer(), 30 * ticksPerSecond);
            return;
        }
    }

    @EventHandler
    public void asyncPlayerChatEventCensor(AsyncPlayerChatEvent event) {
        String message = event.getMessage();
        List<String> holyWords = plugin.getConfig().getStringList("doom.types.RandoomByChat.holyWords.list");
        if (holyWords.size() == 0)
            return;

        for (String holyWord : holyWords) {
            if (!Pattern.compile("(?i).*" + holyWord + ".*").matcher(message).matches())
                continue;

            List<String> replaceables = plugin.getConfig().getStringList("doom.types.RandoomByChat.holyWords.replaceables");
            if (replaceables.size() == 0)
                return;

            message = message.replaceAll("(?i)" + holyWord, replaceables.get((int) (Math.random() * replaceables.size())));
        }
        event.setMessage(message);
    }
}
