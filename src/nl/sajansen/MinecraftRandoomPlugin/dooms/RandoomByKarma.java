package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;
import static nl.sajansen.MinecraftRandoomPlugin.Main.ticksPerSecond;

/**
 * Initiator for RandoomByKarmaListener
 */
public class RandoomByKarma extends RandoomClass {

    private static RandoomByKarmaListener currentListener;

    public RandoomByKarma(JavaPlugin plugin) {
        super(plugin);
    }

    @Override
    public String getName() {
        return "DOOM by Karma";
    }

    @Override
    public void run() {
        if (!isDoomEnabled()) {
            return;
        }

        if (currentListener != null) {
            currentListener.unregister();
        }

        currentListener = new RandoomByKarmaListener(plugin);
        currentListener.register();

        int randoomDuration = plugin.getConfig().getInt("doom.types.RandoomByKarma.duration") * ticksPerSecond;
        currentListener.setMaxDuration(randoomDuration);

        plugin.getServer().broadcastMessage(ChatColor.RED + "DOOM knows karma!");
    }
}
