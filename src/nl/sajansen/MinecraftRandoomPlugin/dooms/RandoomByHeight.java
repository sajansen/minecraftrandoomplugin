package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;
import static nl.sajansen.MinecraftRandoomPlugin.Main.ticksPerSecond;

/**
 * Every player must get to a height above their surrounding in order to not get melted by lava
 */
public class RandoomByHeight extends RandoomClass {

    private int timeLeft = 0;

    public RandoomByHeight(JavaPlugin plugin) {
        super(plugin);
    }

    @Override
    public String getName() {
        return "DOOM by Height";
    }

    @Override
    public void run() {
        if (!isDoomEnabled()) {
            return;
        }

        timeLeft = plugin.getConfig().getInt("doom.types.RandoomByHeight.timeout") * ticksPerSecond;
        plugin.getServer().broadcastMessage(ChatColor.RED + "The floor is LAVA!");
        timeLeftTick();
        playSpawnDoomSound();
    }

    private void timeLeftTick() {
        if (!isDoomEnabled()) {
            return;
        }

        if (timeLeft <= 0) {
            timesUp();
            return;
        }

        plugin.getServer().broadcastMessage(ChatColor.RED + String.valueOf(timeLeft / ticksPerSecond) + "...");
        timeLeft -= ticksPerSecond;

        new BukkitRunnable() {
            @Override
            public void run() {
                timeLeftTick();
            }
        }.runTaskLater(plugin, ticksPerSecond);
    }

    private void timesUp() {
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            playDoomSound(player);
        }

        int radius = plugin.getConfig().getInt("doom.types.RandoomByHeight.radius");
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            if (playerIsFromTheGround(player, radius))
                continue;

            spawnLavaAtPlayer(player);
        }
    }

    private boolean playerIsFromTheGround(Player player, int radius) {
        Location playerLocation = player.getLocation();
        for (int x = playerLocation.getBlockX() - radius; x <= playerLocation.getBlockX() + radius; x++) {
            for (int z = playerLocation.getBlockZ() - radius; z <= playerLocation.getBlockZ() + radius; z++) {
                if (x == playerLocation.getBlockX() && z == playerLocation.getBlockZ())
                    continue;

                for (int y = playerLocation.getBlockY() - 1; y <= playerLocation.getBlockY(); y++) {
                    Block block = player.getWorld().getBlockAt(x, y, z);
                    if (block.getType().equals(Material.AIR))
                        continue;

                    return false;
                }
            }
        }
        return true;
    }

}
