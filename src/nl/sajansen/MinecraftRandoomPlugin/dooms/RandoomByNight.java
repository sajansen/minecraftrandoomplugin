package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;
import static nl.sajansen.MinecraftRandoomPlugin.Main.ticksPerSecond;

/**
 * Every player will be teleported to the surface to get a chance to enjoy the stars.
 * If they don't enjoy it, Doom will be around
 */
public class RandoomByNight extends RandoomClass {

    public RandoomByNight(JavaPlugin plugin) {
        super(plugin);
    }

    @Override
    public String getName() {
        return "DOOM by Night";
    }

    @Override
    public void run() {
        if (!isDoomEnabled()) {
            return;
        }

        // Set time to the begin of the night
        for (World world : plugin.getServer().getWorlds()) {
            world.setTime(14000);
        }

        // Move each player to the surface so they can enjoy the stars
        for (Player player : plugin.getServer().getOnlinePlayers()) {
            Location newLocation = player.getLocation();
            newLocation.setY(player.getWorld().getHighestBlockYAt(player.getLocation()));
            player.teleport(newLocation);
        }

        plugin.getServer().broadcastMessage(ChatColor.RED + "Look at those stars! LOOK AT THEM!");

        // Give some real DOOM after a silent timeout
        int mobTimeout = plugin.getConfig().getInt("doom.types.RandoomByNight.spawnMobs.timeout") * ticksPerSecond;
        new BukkitRunnable() {
            @Override
            public void run() {
                giveRealDoomToPlayers();
            }
        }.runTaskLater(plugin, mobTimeout);
    }

    private void giveRealDoomToPlayers() {
        int radius = plugin.getConfig().getInt("doom.types.RandoomByNight.spawnMobs.radius");
        int duration = plugin.getConfig().getInt("doom.types.RandoomByNight.spawnMobs.duration") * ticksPerSecond;
        int minMobAmount = plugin.getConfig().getInt("doom.types.RandoomByNight.spawnMobs.amount.min");
        int maxMobAmount = plugin.getConfig().getInt("doom.types.RandoomByNight.spawnMobs.amount.max");

        for (Player player : plugin.getServer().getOnlinePlayers()) {
            // Check if player is looking at the stars
            if (player.getLocation().getPitch() == -90) {
                player.sendMessage(ChatColor.GOLD + "Good boy...");
                continue;
            }

            int mobAmount = minMobAmount + ((int) (Math.random() * (maxMobAmount - minMobAmount)));
            spawnMobsOnPlayer(player, EntityType.ZOMBIE, mobAmount, radius, duration);
            playDoomSound(player);
        }
    }

    @Override
    public void schedule() {
        super.schedule();
        playSpawnDoomSound();
    }
}
