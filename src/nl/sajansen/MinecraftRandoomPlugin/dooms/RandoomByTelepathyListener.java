package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;

/**
 * A random player will receive all the damage other players take. They won't receive any damage
 */
public class RandoomByTelepathyListener extends RandoomListener {

    private final Player doomPlayer;

    public RandoomByTelepathyListener(JavaPlugin plugin, Player doomPlayer) {
        super(plugin);
        this.doomPlayer = doomPlayer;
    }

    @Override
    public String getName() {
        return "RandoomByTelepathyListener";
    }

    @EventHandler
    public void entityDamageEvent(EntityDamageEvent event) {
        if (!isDoomEnabled()) {
            unregister();
            return;
        }

        if (!(event.getEntity() instanceof Player))
            return;

        Player player = (Player) event.getEntity();
        if (player.equals(doomPlayer))
            return;

        if (!plugin.getServer().getOnlinePlayers().contains(doomPlayer)) {
            return;
        }

        player.setHealth(player.getHealth() + event.getDamage());
        doomPlayer.damage(event.getDamage());
        doomPlayer.sendMessage(ChatColor.WHITE + player.getDisplayName() + ChatColor.YELLOW + " says thank you!");
    }

    @EventHandler
    public void playerQuitEvent(PlayerQuitEvent event) {
        if (!isDoomEnabled()) {
            unregister();
            return;
        }

        Player player = event.getPlayer();
        if (!player.equals(doomPlayer))
            return;

        plugin.getServer().broadcastMessage(ChatColor.YELLOW + player.getDisplayName() + ChatColor.LIGHT_PURPLE + " has balls the size of his mamma's cuteness factor: 0");
    }

    @EventHandler
    public void playerLoginEvent(PlayerLoginEvent event) {
        if (!isDoomEnabled()) {
            unregister();
            return;
        }

        Player player = event.getPlayer();
        if (!player.equals(doomPlayer))
            return;

        plugin.getServer().broadcastMessage(ChatColor.LIGHT_PURPLE + "Look who decided to show up");
    }
}
