package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;

/**
 * An unknown random block type will cause DOOM to the player destroying it
 */
public class RandoomByBlockListener extends RandoomListener {

    private final RandoomByBlock caller;
    private final ArrayList<Material> doomBlockMaterials;

    public RandoomByBlockListener(JavaPlugin plugin, RandoomByBlock caller, ArrayList<Material> doomBlockMaterials) {
        super(plugin);
        this.caller = caller;
        this.doomBlockMaterials = doomBlockMaterials;
    }

    @Override
    public String getName() {
        return "RandoomByBlockListener";
    }

    @EventHandler
    public void blockBreakEvent(BlockBreakEvent event) {
        if (!isDoomEnabled()) {
            unregister();
            return;
        }

        if (!doomBlockMaterials.contains(event.getBlock().getType()))
            return;

        unregister();

        Player player = event.getPlayer();
        plugin.getServer().broadcastMessage(ChatColor.GREEN + "Congratulations, " + ChatColor.YELLOW + player.getDisplayName() + ChatColor.RED + " has found the DOOM!");

        caller.playDoomSound(player);
        TNTPrimed blowableBlock = event.getBlock().getWorld().spawn(event.getBlock().getLocation(), TNTPrimed.class);
        blowableBlock.setFuseTicks(0);
        caller.explodePlayer(player, 2F);
    }
}
