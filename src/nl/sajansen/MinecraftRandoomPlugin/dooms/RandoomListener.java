package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

/**
 * All doom listeners must extend this class
 */
abstract public class RandoomListener implements Listener {

    protected final JavaPlugin plugin;
    protected BukkitTask timeoutTask;

    public RandoomListener(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    abstract public String getName();

    public void register() {
        plugin.getLogger().fine("Registering " + getName());
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    public void unregister() {
        plugin.getLogger().fine("Unregistering " + getName());
        HandlerList.unregisterAll(this);
        if (timeoutTask != null) {
            timeoutTask.cancel();
        }
    }

    public void setMaxDuration(int duration) {
        timeoutTask = new BukkitRunnable() {
            @Override
            public void run() {
                plugin.getLogger().fine( getName() + " timed out");
                unregister();
            }
        }.runTaskLater(plugin, duration);
    }
}
