package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;
import static nl.sajansen.MinecraftRandoomPlugin.Main.ticksPerSecond;

/**
 * Initiator for RandoomByDiseaseListener
 */
public class RandoomByDisease extends RandoomClass {

    private static ArrayList<RandoomByDiseaseListener> currentListeners = new ArrayList<>();

    public RandoomByDisease(JavaPlugin plugin) {
        super(plugin);
    }

    @Override
    public String getName() {
        return "DOOM by Disease";
    }

    @Override
    public void run() {
        if (!isDoomEnabled()) {
            return;
        }

        doomPlayer = getRandomPlayer();
        if (doomPlayer == null)
            return;

        if (checkIfPlayerIsAlreadyDoomed(doomPlayer)) {
            plugin.getLogger().fine("Random player is already doomed");
            return;
        }

        RandoomByDiseaseListener listener = new RandoomByDiseaseListener(plugin, this, doomPlayer);
        listener.register();

        int randoomDuration = plugin.getConfig().getInt("doom.types.RandoomByDisease.duration") * ticksPerSecond;
        listener.setDuration(randoomDuration);
        listener.setMaxDuration(randoomDuration + 100);

        plugin.getServer().broadcastMessage(ChatColor.YELLOW + doomPlayer.getDisplayName() + ChatColor.RED + " has been infected!");
        listener.startDisease();

        currentListeners.add(listener);
    }

    private boolean checkIfPlayerIsAlreadyDoomed(Player player) {
        for (RandoomByDiseaseListener listener : currentListeners) {
            if (player.equals(listener.getDoomPlayer()))
                return true;
        }
        return false;
    }

    public void removeCurrentListener(RandoomByDiseaseListener listener) {
        currentListeners.remove(listener);
    }
}
