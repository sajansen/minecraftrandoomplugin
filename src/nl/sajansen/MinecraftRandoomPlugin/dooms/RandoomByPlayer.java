package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;
import static nl.sajansen.MinecraftRandoomPlugin.Main.ticksPerSecond;

/**
 * After an known time duration, an random unknown player will receive DOOM
 */
public class RandoomByPlayer extends RandoomClass {

    public RandoomByPlayer(JavaPlugin plugin) {
        super(plugin);
    }

    @Override
    public String getName() {
        return "DOOM by Player";
    }

    @Override
    public void run() {
        if (!isDoomEnabled()) {
            return;
        }

        doomPlayer = getRandomPlayer();
        if (doomPlayer == null)
            return;

        plugin.getServer().broadcastMessage(ChatColor.RED + "DOOM has arrived for the unlucky " + ChatColor.YELLOW + doomPlayer.getDisplayName() + ChatColor.RED + "!");
        killPlayerWithRandomEffects(doomPlayer);
    }

    @Override
    public void schedule() {
        super.schedule();
        plugin.getServer().broadcastMessage(ChatColor.RED + "Someone will get some DOOM in " + ChatColor.YELLOW + (doomTime / ticksPerSecond) + ChatColor.RED + " seconds!");
        playSpawnDoomSound();
    }
}
