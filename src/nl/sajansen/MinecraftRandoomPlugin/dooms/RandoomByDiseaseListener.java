package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;
import static nl.sajansen.MinecraftRandoomPlugin.Main.ticksPerSecond;

/**
 * A random player gets the disease. He can give the disease to another player.
 * The player having the disease when the time is up, will be doomed
 */
public class RandoomByDiseaseListener extends RandoomListener {

    private final RandoomByDisease caller;
    private Player doomPlayer;
    private int timeLeft = 0;
    private boolean isTik = true;
    private final PotionEffectType potionEffectType = PotionEffectType.CONDUIT_POWER;
    private BukkitTask timer;

    public RandoomByDiseaseListener(JavaPlugin plugin, RandoomByDisease caller, Player doomPlayer) {
        super(plugin);
        this.doomPlayer = doomPlayer;
        this.caller = caller;
    }

    @Override
    public String getName() {
        return "RandoomByDiseaseListener";
    }

    @EventHandler
    public void playerInteractEntityEvent(PlayerInteractEntityEvent event) {
        if (!isDoomEnabled()) {
            unregister();
            return;
        }

        if (!(event.getRightClicked() instanceof Player))
            return;

        Player player = (Player) event.getRightClicked();
        if (player.equals(doomPlayer))
            return;

        curePlayer(doomPlayer);
        diseasePlayer(player);
        plugin.getServer().broadcastMessage(ChatColor.RED + "Disease has spread to " + ChatColor.YELLOW + player.getDisplayName());
    }

    @Override
    public void unregister() {
        super.unregister();
        caller.removeCurrentListener(this);
    }

    public Player getDoomPlayer() {
        return doomPlayer;
    }

    public void setDuration(int duration) {
        timeLeft = duration;
    }

    public void startDisease() {
        this.timeLeftTick();
        diseasePlayer(doomPlayer);
        caller.playSpawnDoomSound();
    }

    private void curePlayer(Player player) {
        player.removePotionEffect(potionEffectType);
    }

    private void diseasePlayer(Player player) {
        PotionEffect potionEffect = new PotionEffect(potionEffectType, timeLeft, 2);
        player.addPotionEffect(potionEffect, true);
        caller.playDoomSound(player);
        doomPlayer = player;
    }

    private void timeLeftTick() {
        if (!isDoomEnabled()) {
            return;
        }

        if (timeLeft <= 0) {
            timesUp();
            return;
        }

        plugin.getLogger().fine((timeLeft / ticksPerSecond) + " seconds...");
        plugin.getServer().broadcastMessage(ChatColor.GRAY + (isTik ? "tik..." : "tok..."));
        isTik = !isTik;

        int tickInterval;
        if (timeLeft <= 7 * ticksPerSecond) {
            tickInterval = (int) (0.5 * ticksPerSecond);
        } else if (timeLeft <= 15 * ticksPerSecond) {
            tickInterval = ticksPerSecond;
        } else if (timeLeft <= 30 * ticksPerSecond) {
            tickInterval = 3 * ticksPerSecond;
        } else if (timeLeft <= 60 * ticksPerSecond) {
            tickInterval = 4 * ticksPerSecond;
        } else {
            tickInterval = 5 * ticksPerSecond;
        }

        timeLeft -= tickInterval;

        timer = new BukkitRunnable() {
            @Override
            public void run() {
                timeLeftTick();
            }
        }.runTaskLater(plugin, tickInterval);
    }

    private void timesUp() {
        curePlayer(doomPlayer);
        caller.setPlayerOnFire(doomPlayer);
        unregister();
    }

    @EventHandler
    public void playerDeathEvent(PlayerDeathEvent event) {
        if (!isDoomEnabled()) {
            unregister();
            return;
        }

        Player player = event.getEntity();
        if (!player.equals(doomPlayer))
            return;

        plugin.getServer().broadcastMessage(ChatColor.YELLOW + player.getDisplayName() + ChatColor.LIGHT_PURPLE + " has sacrificed himself in order to end the DOOM");
        unregister();
        if (timer != null)
            timer.cancel();
    }

    @EventHandler
    public void playerQuitEvent(PlayerQuitEvent event) {
        if (!isDoomEnabled()) {
            unregister();
            return;
        }

        Player player = event.getPlayer();
        if (!player.equals(doomPlayer))
            return;

        plugin.getServer().broadcastMessage(ChatColor.YELLOW + player.getDisplayName() + ChatColor.LIGHT_PURPLE + " has run to his mommy :'(");
        curePlayer(player);
    }

    @EventHandler
    public void playerLoginEvent(PlayerLoginEvent event) {
        if (!isDoomEnabled()) {
            unregister();
            return;
        }

        Player player = event.getPlayer();
        if (!player.equals(doomPlayer))
            return;

        plugin.getServer().broadcastMessage(ChatColor.LIGHT_PURPLE + "Look who is back >:)");
        diseasePlayer(player);
    }
}
