package nl.sajansen.MinecraftRandoomPlugin.dooms;

import com.google.common.collect.Iterables;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static nl.sajansen.MinecraftRandoomPlugin.Main.ticksPerSecond;

/**
 * All Dooms need to extend this class
 */
abstract public class RandoomClass extends BukkitRunnable {

    ArrayList<EntityType> dangerousMobs = new ArrayList<>(Arrays.asList(
            EntityType.CREEPER,
            EntityType.CAVE_SPIDER,
            EntityType.GHAST));

    protected final JavaPlugin plugin;
    protected Player doomPlayer = null;
    protected int doomTime = 0;

    public RandoomClass(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    abstract public String getName();

    public void schedule() {
        int doomFireTimeoutMin = plugin.getConfig().getInt("doom.spawntime.min") * ticksPerSecond;
        int doomFireTimeoutMax = plugin.getConfig().getInt("doom.spawntime.max") * ticksPerSecond;
        doomTime = doomFireTimeoutMin + (int) (Math.random() * (doomFireTimeoutMax - doomFireTimeoutMin));

        this.runTaskLater(plugin, doomTime);
    }

    public int getDoomTime() {
        return doomTime;
    }

    public Player getDoomPlayer() {
        return doomPlayer;
    }

    protected Player getRandomPlayer() {
        Collection<? extends Player> players = Bukkit.getServer().getOnlinePlayers();
        if (players.size() == 0)
            return null;

        doomPlayer = Iterables.get(players, (int) (Math.random() * players.size()));
        if (doomPlayer == null) {
            plugin.getLogger().severe("Random player is null!");
            return null;
        }

        return doomPlayer;
    }

    public void killPlayerWithRandomEffects(Player player) {
        int doomChoice = (int) (Math.random() * 7);
        if (doomChoice < 1) {
            playerStrikeLightning(player);
        } else if (doomChoice < 2) {
            setPlayerOnFire(player);
        } else if (doomChoice < 3) {
            spawnLavaAtPlayer(player);
        } else if (doomChoice < 4) {
            spawnDangerousMobsOnPlayer(player);
        } else if (doomChoice < 5) {
            explodePlayer(player);
        } else if (doomChoice < 6) {
            robPlayer(player);
        } else {
            player.damage(player.getHealth());
        }

        playDoomSound(player);
    }

    void playerStrikeLightning(Player player) {
        plugin.getLogger().fine("Applying DOOM to " + player.getDisplayName() + ": playerStrikeLightning");
        player.getWorld().strikeLightning(player.getLocation());
    }

    void explodePlayer(Player player) {
        explodePlayer(player, 2F);
    }

    void explodePlayer(Player player, float explosionPower) {
        plugin.getLogger().fine("Applying DOOM to " + player.getDisplayName() + ": explodePlayer");
        player.getWorld().createExplosion(player.getLocation(), explosionPower);
    }

    public void setPlayerOnFire(Player player) {
        int duration = 5 * ticksPerSecond + (int) (Math.random() * 30 * ticksPerSecond);
        setPlayerOnFire(player, duration);
    }

    public void setPlayerOnFire(Player player, int duration) {
        plugin.getLogger().fine("Applying DOOM to " + player.getDisplayName() + ": setPlayerOnFire");
        player.setFireTicks(duration);
    }

    void spawnDangerousMobsOnPlayer(Player player) {
        plugin.getLogger().fine("Applying DOOM to " + player.getDisplayName() + ": spawnDangerousMobsOnPlayer");

        int radius = plugin.getConfig().getInt("doom.effects.spawnDangerousMobsOnPlayer.radius");
        int minAmountOfMobs = plugin.getConfig().getInt("doom.effects.spawnDangerousMobsOnPlayer.amount.min");
        int maxAmountOfMobs = plugin.getConfig().getInt("doom.effects.spawnDangerousMobsOnPlayer.amount.max");
        int selfDestructionTimeout = plugin.getConfig().getInt("doom.effects.spawnDangerousMobsOnPlayer.duration");
        int numberOfSpawnedEntities = (int) (Math.random() * (maxAmountOfMobs - minAmountOfMobs)) + minAmountOfMobs;

        EntityType choosenDangerousMob = dangerousMobs.get((int) (Math.random() * dangerousMobs.size()));

        spawnMobsOnPlayer(player, choosenDangerousMob, numberOfSpawnedEntities, radius, selfDestructionTimeout * ticksPerSecond);
    }

    void spawnMobsOnPlayer(Player player, EntityType entityType, int amount, int radius, int selfDestructionTimeout) {
        // Create Mobs
        ArrayList<Entity> spawnedEntities = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            Location spawnLocation = getRandomLocationNearbyPlayer(player, radius);
            Entity spawnedEntity = player.getWorld().spawnEntity(spawnLocation, entityType);
            spawnedEntities.add(i, spawnedEntity);
        }

        if (selfDestructionTimeout == 0)
            return;

        // Set self destruction timer on Mobs
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Entity spawnedEntity : spawnedEntities) {
                    spawnedEntity.getWorld().createExplosion(spawnedEntity.getLocation(), 0.5F);    // Go out with a Bang!
                    spawnedEntity.remove();
                }
            }
        }.runTaskLater(plugin, selfDestructionTimeout);
    }

    protected Location getRandomLocationNearbyPlayer(Player player, int radius) {
        Location spawnLocation = player.getLocation();
        spawnLocation.setX((spawnLocation.getX() - radius) + Math.random() * 2 * radius);
        spawnLocation.setZ((spawnLocation.getZ() - radius) + Math.random() * 2 * radius);

        // Decide where to spawn nearby the player
        int maxIterations = radius * 2;
        while (!Material.AIR.equals(player.getWorld()
                .getBlockAt(spawnLocation)
                .getType())) {
            if (maxIterations-- < 0)
                break;

            // Use the location of the block around the player with a small height difference
            Block highestBlock = player.getWorld().getHighestBlockAt(spawnLocation);
            if (highestBlock.getY() < player.getLocation().getY() + 3 && highestBlock.getY() > player.getLocation().getY() - 2) {
                spawnLocation.setY(highestBlock.getY());
                break;
            }

            // Try a block one step closer to the player
            spawnLocation.setX(spawnLocation.getX() + (player.getLocation().getX() > spawnLocation.getX() ? 1 : -1) * ((int) (Math.random() * 2)));
            spawnLocation.setZ(spawnLocation.getZ() + (player.getLocation().getZ() > spawnLocation.getZ() ? 1 : -1) * ((int) (Math.random() * 2)));

            // Just spawn on the player already!
            if (spawnLocation.getBlockX() == player.getLocation().getBlockX() && spawnLocation.getBlockZ() == player.getLocation().getBlockZ())
                break;
        }
        return spawnLocation;
    }

    void spawnLavaAtPlayer(Player player) {
        plugin.getLogger().fine("Applying DOOM to " + player.getDisplayName() + ": spawnLavaAtPlayer");
        Block block = player.getLocation().getBlock();
        Material originalMaterial = block.getType();
        block.setType(Material.LAVA);
        player.sendMessage(ChatColor.YELLOW + "THE FLOOR IS LAVA! Muwhahaha");

        new BukkitRunnable() {
            @Override
            public void run() {
                block.setType(originalMaterial);
            }
        }.runTaskLater(plugin, 4 * ticksPerSecond);
    }

    void robPlayer(Player player) {
        int minDamage = plugin.getConfig().getInt("doom.effects.robPlayer.playerDamage.min");
        int maxDamage = plugin.getConfig().getInt("doom.effects.robPlayer.playerDamage.max");

        plugin.getLogger().fine("Applying DOOM to " + player.getDisplayName() + ": robPlayer");
        player.damage(minDamage + Math.random() * (maxDamage - minDamage));
        PlayerInventory inventory = player.getInventory();

        if (inventory.getSize() == 0) {
            player.sendMessage(ChatColor.WHITE + "<Robber> " + ChatColor.YELLOW + "You twat, ain't even got stuff on you!");
            player.damage(minDamage + Math.random() * (maxDamage - minDamage));  // Hit again
            return;
        }

        for (ItemStack itemStack : inventory) {
            if (itemStack == null)
                continue;

            int newAmount = itemStack.getAmount();
            newAmount -= itemStack.getAmount() / 2.0;
            newAmount -= Math.random() * newAmount;
            newAmount = Math.max(0, newAmount);
            plugin.getLogger().finer("Stealing " + (itemStack.getAmount() - newAmount) + "x "
                                    + itemStack.getType().toString() + " from player");

            itemStack.setAmount(newAmount);
        }
        player.sendMessage(ChatColor.WHITE + "<Robber> " + ChatColor.YELLOW + "Thanks for your stuff! Ghehehe...");
    }

    public void playDoomSound(Player player) {
        player.getWorld().playSound(player.getLocation(), Sound.ENTITY_GHAST_HURT, 10, 1);
    }

    public void playSpawnDoomSound() {
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            player.getWorld().playSound(player.getLocation(), Sound.AMBIENT_CAVE, 7, 1.5F);
        }
    }
}
