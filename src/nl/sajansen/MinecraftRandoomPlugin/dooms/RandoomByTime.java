package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;

/**
 * After an unknown time duration, an random known player will receive DOOM
 */
public class RandoomByTime extends RandoomClass {

    public RandoomByTime(JavaPlugin plugin) {
        super(plugin);

        doomPlayer = getRandomPlayer();
    }

    @Override
    public String getName() {
        return "DOOM by Time";
    }

    @Override
    public void run() {
        if (!isDoomEnabled()) {
            return;
        }

        if (!plugin.getServer().getOnlinePlayers().contains(doomPlayer)) {
            plugin.getServer().broadcastMessage(ChatColor.YELLOW + doomPlayer.getDisplayName() + ChatColor.RED + ", you chicken!");
            return;
        }

        plugin.getServer().broadcastMessage(ChatColor.YELLOW + doomPlayer.getDisplayName() + ChatColor.RED + ", your DOOM has arrived!");
        killPlayerWithRandomEffects(doomPlayer);
    }

    @Override
    public void schedule() {
        if (doomPlayer == null)
            return;

        super.schedule();
        plugin.getServer().broadcastMessage(ChatColor.RED + "DOOM will be arriving " + ChatColor.YELLOW + doomPlayer.getDisplayName() + ChatColor.RED + "!");
        playSpawnDoomSound();
    }
}
