package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;
import static nl.sajansen.MinecraftRandoomPlugin.Main.ticksPerSecond;

/**
 * Initiator for RandoomByBlockListener
 */
public class RandoomByBlock extends RandoomClass {

    private ArrayList<ArrayList<Material>> doomMaterials = new ArrayList<>(Arrays.asList(
            new ArrayList<>(Arrays.asList(Material.GRASS_BLOCK,
                                          Material.DIRT,
                                          Material.COARSE_DIRT)),
            new ArrayList<>(Arrays.asList(Material.SAND,
                                          Material.GRAVEL)),
            new ArrayList<>(Arrays.asList(Material.GLASS, Material.GLASS_PANE)),
            new ArrayList<>(Arrays.asList(Material.OAK_LOG,
                                                  Material.JUNGLE_LOG,
                                                  Material.ACACIA_LOG,
                                                  Material.BIRCH_LOG,
                                                  Material.DARK_OAK_LOG,
                                                  Material.SPRUCE_LOG)),
            new ArrayList<>(Arrays.asList(Material.OAK_LEAVES,
                                                  Material.JUNGLE_LEAVES,
                                                  Material.ACACIA_LEAVES,
                                                  Material.BIRCH_LEAVES,
                                                  Material.DARK_OAK_LEAVES,
                                                  Material.SPRUCE_LEAVES)),
            new ArrayList<>(Arrays.asList(Material.CACTUS)),
            new ArrayList<>(Arrays.asList(Material.WHEAT,
                                          Material.WHEAT_SEEDS,
                                          Material.GRASS,
                                          Material.CARROTS,
                                          Material.PUMPKIN)),
            new ArrayList<>(Arrays.asList(Material.STONE,
                                          Material.COBBLESTONE))));

    public RandoomByBlock(JavaPlugin plugin) {
        super(plugin);
    }

    @Override
    public String getName() {
        return "DOOM by Block";
    }

    @Override
    public void run() {
        if (!isDoomEnabled()) {
            return;
        }
        RandoomByBlockListener listener = new RandoomByBlockListener(plugin, this, getRandomBlockMaterials());
        listener.register();

        int randoomDuration = plugin.getConfig().getInt("doom.types.RandoomByBlock.duration") * ticksPerSecond;
        listener.setMaxDuration(randoomDuration);

        plugin.getServer().broadcastMessage(ChatColor.RED + "DOOM has hidden itself!");
    }

    private ArrayList<Material> getRandomBlockMaterials() {
        return doomMaterials.get((int) (Math.random() * doomMaterials.size()));
    }
}
