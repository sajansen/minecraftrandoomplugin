package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.plugin.java.JavaPlugin;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;

/**
 * Every player will receive the damage they give to another entity
 */
public class RandoomByKarmaListener extends RandoomListener {

    public RandoomByKarmaListener(JavaPlugin plugin) {
        super(plugin);
    }

    @Override
    public String getName() {
        return "RandoomByKarmaListener";
    }

    @EventHandler
    public void entityDamageEvent(EntityDamageByEntityEvent event) {
        if (!isDoomEnabled()) {
            unregister();
            return;
        }

        if (!(event.getDamager() instanceof Player))
            return;

        handleDamageEvent((Player) event.getDamager(), event.getDamage());
    }

    private void handleDamageEvent(Player player, double damage) {
        player.damage(damage);
    }
}
