package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;
import static nl.sajansen.MinecraftRandoomPlugin.Main.ticksPerSecond;

/**
 * Initiator for RandoomByTelepathyListener
 */
public class RandoomByTelepathy extends RandoomClass {

    private static RandoomByTelepathyListener currentListener;

    public RandoomByTelepathy(JavaPlugin plugin) {
        super(plugin);
    }

    @Override
    public String getName() {
        return "DOOM by Telepathy";
    }

    @Override
    public void run() {
        if (!isDoomEnabled()) {
            return;
        }

        if (currentListener != null) {
            currentListener.unregister();
        }

        doomPlayer = getRandomPlayer();
        if (doomPlayer == null)
            return;

        currentListener = new RandoomByTelepathyListener(plugin, doomPlayer);
        currentListener.register();

        int randoomDuration = plugin.getConfig().getInt("doom.types.RandoomByTelepathy.duration") * ticksPerSecond;
        currentListener.setMaxDuration(randoomDuration);

        plugin.getServer().broadcastMessage(ChatColor.YELLOW + doomPlayer.getDisplayName() + ChatColor.RED + " has volunteered to receive everyone else's DOOM!");
    }
}
