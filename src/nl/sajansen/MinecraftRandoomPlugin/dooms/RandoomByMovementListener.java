package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;

/**
 * The player may get doomed by setting the wrong step. This Doom will help them to be teleported
 * to random nearby locations on every step they take
 */
public class RandoomByMovementListener extends RandoomListener {

    private final RandoomByMovement caller;
    private double teleportChance;

    public RandoomByMovementListener(JavaPlugin plugin, RandoomByMovement caller) {
        super(plugin);
        this.caller = caller;
        teleportChance = plugin.getConfig().getDouble("doom.types.RandoomByMovement.teleport.chance");
    }

    @Override
    public String getName() {
        return "RandoomByMovementListener";
    }

    @EventHandler
    public void playerMoveEvent(PlayerMoveEvent event) {
        if (!isDoomEnabled()) {
            unregister();
            return;
        }

        if (event.getTo() == null)
            return;

        Location from = event.getFrom();
        Location to = event.getTo();

        if (!playerHasMoved(from, to))
            return;

        double random = Math.random();
        if (random >= teleportChance)
            return;

        int radius = plugin.getConfig().getInt("doom.types.RandoomByMovement.teleport.radius");
        event.getPlayer().teleport(caller.getRandomLocationNearbyPlayer(event.getPlayer(), radius));
    }

    private boolean playerHasMoved(Location from, Location to) {
        return !(from.getX() == to.getX()
                && from.getY() == to.getY()
                && from.getZ() == to.getZ());
    }

}
