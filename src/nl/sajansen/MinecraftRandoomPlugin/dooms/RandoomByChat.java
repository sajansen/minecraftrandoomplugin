package nl.sajansen.MinecraftRandoomPlugin.dooms;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * Initiator for RandoomByChatListener
 */
public class RandoomByChat extends RandoomClass{
    private final RandoomByChatListener listener;

    public RandoomByChat(JavaPlugin plugin) {
        super(plugin);
        listener = new RandoomByChatListener(plugin, this);
    }

    @Override
    public void run() {}

    @Override
    public String getName() {
        return "RandoomByChat";
    }

    public void enable() {
        listener.register();
    }

    public void disable() {
        listener.unregister();
    }
}
