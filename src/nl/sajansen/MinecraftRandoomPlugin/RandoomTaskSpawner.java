package nl.sajansen.MinecraftRandoomPlugin;

import nl.sajansen.MinecraftRandoomPlugin.dooms.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collection;

import static nl.sajansen.MinecraftRandoomPlugin.Main.isDoomEnabled;
import static nl.sajansen.MinecraftRandoomPlugin.Main.ticksPerSecond;

/**
 * This runnable takes care for creating new Dooms, which will be fired after their own DoomTime
 */
public class RandoomTaskSpawner extends BukkitRunnable {

    private final JavaPlugin plugin;
    private static RandoomClass currentDoom;

    public RandoomTaskSpawner(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    public static void start(JavaPlugin plugin) {
        new RandoomTaskSpawner(plugin).schedule();
    }

    @Override
    public void run() {
        if (!isDoomEnabled()) {
            return;
        }
        spawnDoom();
        schedule();
    }

    private void spawnDoom() {
        Collection<? extends Player> players = Bukkit.getServer().getOnlinePlayers();
        if (players.size() == 0) {
            return;
        }

        int doomChoice = (int) (Math.random() * 10);
        switch (doomChoice) {
            case 0:
                currentDoom = new RandoomByPlayer(plugin);
                break;
            case 1:
                currentDoom = new RandoomByTime(plugin);
                break;
            case 2:
                currentDoom = new RandoomByBlock(plugin);
                break;
            case 3:
                currentDoom = new RandoomByDisease(plugin);
                break;
            case 4:
                currentDoom = new RandoomByHeight(plugin);
                break;
            case 5:
                currentDoom = new RandoomByKarma(plugin);
                break;
            case 6:
                currentDoom = new RandoomByTelepathy(plugin);
                break;
            case 7:
                currentDoom = new RandoomByNight(plugin);
                break;
            case 8:
                currentDoom = new RandoomByMovement(plugin);
                break;
            case 9:
                currentDoom = new RandoomByGravity(plugin);
                break;
            default:
                plugin.getLogger().warning("Somehow my doomchoice is out of bounds");
                return;
        }

        currentDoom.schedule();

        plugin.getLogger().info("DOOM '" + currentDoom.getName()
                                + "' will be received in " + (currentDoom.getDoomTime() / ticksPerSecond) + " seconds"
                                + (currentDoom.getDoomPlayer() != null ? " for player " + currentDoom.getDoomPlayer().getDisplayName() : ""));
    }

    private void schedule() {
        int nextDoomCreationTimeoutMin = plugin.getConfig().getInt("doom.nextcreationtime.min") * ticksPerSecond;
        int nextDoomCreationTimeoutMax = plugin.getConfig().getInt("doom.nextcreationtime.max") * ticksPerSecond;

        int newDoomSpawnTime = nextDoomCreationTimeoutMin
                               + (currentDoom != null ? currentDoom.getDoomTime() : 0)
                               + (int) (Math.random() * (nextDoomCreationTimeoutMax - nextDoomCreationTimeoutMin));
        new RandoomTaskSpawner(plugin).runTaskLater(plugin, newDoomSpawnTime);
        plugin.getLogger().fine("New DOOM will be generated in " + (newDoomSpawnTime / ticksPerSecond) + " seconds");
    }
}
